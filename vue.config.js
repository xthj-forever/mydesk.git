const path = require("path")
const IgnorePlugin = require("webpack").IgnorePlugin
const resolve = (dir) => path.join(__dirname, dir)
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin
const CompressionPlugin = require("compression-webpack-plugin")
const isAnalyzeMode = process.env.MODE === "analyze"
const isProductionMode = process.env.MODE === "production"

module.exports = {
  lintOnSave: false,
  publicPath: "./",
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [path.resolve(__dirname, "src/styles/variables.less")],
      injector: "prepend",
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("@assets", resolve("src/assets"))
      .set("@components", resolve("src/components"))
      .set("@views", resolve("src/views"))
      .set("@utils", resolve("src/utils"))
      .set("@store", resolve("src/store"))
      .set("@router", resolve("src/router"))
      .set("@service", resolve("src/service"))
      .set("@styles", resolve("src/styles"))
    config.plugin("html").tap((args) => {
      args[0].title = ""
      return args
    })
  },
  configureWebpack: (config) => {
    if (isAnalyzeMode) {
      config.plugins.push(new BundleAnalyzerPlugin())
    }
    config.plugins.push(
      new IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/,
      })
    )
    if (isProductionMode) {
      config.plugins.push(
        new CompressionPlugin({
          algorithm: "gzip",
          test: /\.js$|\.css/,
          threshold: 10240,
        })
      )
    }
    config.optimization.splitChunks.cacheGroups.antdvue = {
      name: 'chunk-a',
      test: /[\\/]node_modules[\\/]ant-design-vue/,
      priority: -5,
      chunks: 'all'
    }
  },
  devServer: {
    proxy: {
      "/api": {
        target: process.env.VUE_APP_BACK_API_PREFIX1,
        changeOrigin: true,
        pathRewrite: {
          "^/api": "",
        },
      },
      "/new": {
        target: process.env.VUE_APP_BACK_API_PREFIX2,
        changeOrigin: true,
        pathRewrite: {
          "^/new": "",
        },
      },
    },
  },
}
