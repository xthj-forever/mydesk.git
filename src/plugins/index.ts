import { Directive, Plugin, ComputedRef, watch, WatchStopHandle } from "vue"
import defaultIconUrl from "@/assets/defaultAppIcon.png"

export const prefixImgUrl: Plugin = (app, prefix: ComputedRef<string>) => {
  function setSrc(el: HTMLImageElement, prefix: string, oVal: string) {
    if (!oVal) {
      el.src = defaultIconUrl
      return
    }
    const hasPrefix = /^(http|https):\/\/.+/.test(oVal)
    if (hasPrefix) {
      el.src = oVal
    } else {
      el.src = `${prefix}/${oVal}`
    }
  }
  const prefixDire: Directive<
    HTMLImageElement & { stop_: WatchStopHandle },
    string
  > = {
    mounted(el, binding) {
      setSrc(el, prefix.value, binding.value)
      el.stop_ && el.stop_()
      el.stop_ = watch(prefix, (newVal) => setSrc(el, newVal, binding.value))
    },
    updated(el, binding) {
      setSrc(el, prefix.value, binding.value)
      el.stop_ && el.stop_()
      el.stop_ = watch(prefix, (newVal) => setSrc(el, newVal, binding.value))
    },
    beforeUnmount(el) {
      el.stop_ && el.stop_()
    },
  }
  app.directive("prefix", prefixDire)
}
