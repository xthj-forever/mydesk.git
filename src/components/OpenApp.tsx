import { render, createVNode, CSSProperties, Ref } from "vue"
import AppModal from "@components/AppModal"
import { app } from "@/main"
import { AppData } from "@/service"

const open = (props: {
  xstyle?: Ref<CSSProperties>
  fill?: Ref<boolean>
  app: AppData
  cbs?: {
    onHide?: () => void
    onClose?: () => void
    onClick?: () => void
  }
}) => {
  const container = document.createDocumentFragment()
  const refmd: any = {}
  const { cbs, ..._props } = props
  const close = () => {
    render(null, container as any)
    cbs?.onClose && cbs?.onClose()
  }

  const vm = createVNode(
    <AppModal onClick={cbs?.onClick} onClose={close} onHide={cbs?.onHide} />,
    { ..._props, refmd }
  )
  vm.appContext = app._context
  render(vm, container as any)

  return {
    close: () => render(null, container as any),
    toggle: (show: boolean) => {
      refmd.value.value.show = show
    },
    style: props.xstyle,
  }
}
export default open
