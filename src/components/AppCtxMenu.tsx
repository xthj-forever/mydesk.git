import {
  defineComponent,
  Teleport,
  PropType,
  render,
  createVNode,
  CSSProperties,
} from "vue"
import styles from "./AppCtxMenu.module.less"
import { useBodyClick } from "@/components/hooks"
import { app } from "@/main"
let _close: () => void
export type Action = { label: string; value: string; [key: string]: any }

const MenuCpt = defineComponent({
  props: {
    actions: Array as PropType<Action[]>,
    customStyle: Object as PropType<CSSProperties>,
  },
  emits: ["action"],
  setup(props, { emit }) {
    useBodyClick(_close)
    const handler = (data: Action) => () => emit("action", data)
    return () => (
      <Teleport to={"body"}>
        <ul class={styles["app-ctx-menue"]} style={props.customStyle}>
          {props.actions?.map((action) => (
            <li class={"item"} onClick={handler(action)}>
              {action.label}
            </li>
          ))}
        </ul>
      </Teleport>
    )
  },
})

export const openAppMenu = (
  actions: Action[],
  handler: (data: Action) => void,
  customStyle: CSSProperties
) => {
  _close && _close()
  _close = () => render(null, container as any)
  const container = document.createDocumentFragment()
  const vm = createVNode(
    <MenuCpt actions={actions} onAction={handler} customStyle={customStyle} />
  )
  vm.appContext = app._context
  render(vm, container as any)
}
