import {
  ref,
  reactive,
  Ref,
  onMounted,
  computed,
  onBeforeUnmount,
  watchPostEffect,
  CSSProperties,
} from "vue"
import { TaskBar } from "@/service"
import { clearApp } from "@/utils/utils"
import { useStore } from "@store/index"
import { openAppMenu, Action } from "@/components/AppCtxMenu"

export const useToggleCareMode = () => {
  const store = useStore()
  const style = computed<CSSProperties>(() => store.getters.careModeScale)
  return style
}

export const useSlide = (list: Ref<any[]>, stepLen = 4) => {
  const containerRef = ref<HTMLDivElement>()
  const currStep = ref(0)
  const appWidth = 44 + 36
  const stepPX = stepLen * appWidth
  const appAllWidth = computed(() => list.value.length * appWidth - 36)
  const containerWidth = ref<number>(9999)

  const showBtn = computed<{ left: boolean; right: boolean }>(() => {
    const res = { left: false, right: false }
    // left btn show
    res.left = currStep.value > 0

    // right btn show
    const allStep = appAllWidth.value / stepPX
    const contStep = containerWidth.value / stepPX
    const disStep = Math.ceil(allStep - contStep)
    res.right = currStep.value < disStep

    return res
  })

  function getContainerWidth() {
    const container = containerRef.value
    if (container) {
      containerWidth.value = container.getBoundingClientRect().width - 20 * 2
    }
  }
  onMounted(getContainerWidth)
  window.addEventListener("resize", getContainerWidth)

  const left = computed(() => `${-currStep.value * stepPX + 20}px`)
  const btnHandler = (dir: "left" | "right") => {
    const map = { left: -1, right: 1 }
    currStep.value += map[dir]
  }

  return {
    containerRef,
    showBtn,
    left,
    btnHandler,
  }
}

export const useMenu = (
  handler: (e: MouseEvent) => void,
  _domRef?: Ref<HTMLElement>
) => {
  const domRef = _domRef || ref<HTMLElement>()
  const posi = reactive<CSSProperties>({ left: "", bottom: "" })

  const _handler: (
    this: HTMLElement,
    ev: HTMLElementEventMap["contextmenu"]
  ) => any = function (e) {
    e.preventDefault()
    handler?.(e)
    if (e.target) {
      const rect = (e.target as HTMLElement).getBoundingClientRect()
      posi.left = `${rect.left - 55}px`
      posi.bottom = `108px`
    }
  }

  onMounted(() => {
    domRef.value?.addEventListener("contextmenu", _handler)
  })
  onBeforeUnmount(() => {
    domRef.value?.removeEventListener("contextmenu", _handler)
  })

  return { domRef, posi }
}

export const useBodyClick = (
  handler: (this: HTMLElement, ev: MouseEvent) => any
) => {
  document.body.addEventListener("click", handler)
  onBeforeUnmount(() => document.body.removeEventListener("click", handler))
}

export const useDragHook = (dropHandler: (data: any) => void) => {
  const dropTip = ref(false)
  const posi = reactive<CSSProperties>({ left: "", bottom: "" })
  const dragover = (e: any) => {
    dropTip.value = true
    if (e.target) {
      const rect = (e.target as HTMLElement).getBoundingClientRect()
      posi.left = `${rect.left - 55}px`
      posi.bottom = `108px`
    }
  }
  const dragleave = () => {
    dropTip.value = false
  }
  const drop = (ev: DragEvent) => {
    const data = ev.dataTransfer?.getData("appData")
    if (data) {
      try {
        const _data = JSON.parse(data)
        dropHandler(_data)
      } catch (e) {
        console.log(e)
      }
    }
    ev.preventDefault()
    dropTip.value = false
  }

  return {
    dropTip,
    dragover,
    dragleave,
    drop,
    posi,
  }
}

export const useTaskBarFixedTip = (list: Ref<TaskBar[]>) => {
  const resolveMenuItems = (data: TaskBar) => {
    const menus = []
    if (data.isOpen) menus.push({ label: "关闭窗口", value: "close" })
    if (data.isLock) {
      menus.push({ label: "从任务栏中解除固定", value: "delete" })
    } else {
      menus.push({ label: "固定到任务栏", value: "add" })
    }
    // menus.push({ label: "取消", value: "cancel" })
    return menus
  }
  const handler = (btn: { label: string; value: string }, data: TaskBar) => {
    const app = list.value.find(
      (app) => app.applicationId === data.applicationId
    )
    if (app) {
      if (btn.value === "delete") {
        app.isLock = false
        if (!app.isOpen) {
          list.value = list.value.filter(
            (app) => app.applicationId !== data.applicationId
          )
        }
      } else if (btn.value === "add") {
        app.isLock = true
      } else if (btn.value === "close") {
        clearApp(app)
      }
    }
  }

  return {
    resolveMenuItems,
    handler,
  }
}

export const useFullTaggle = (editWrapper: Ref<HTMLElement | null>) => {
  const prevPosi: CSSProperties = {}
  const isFull = ref(false)
  watchPostEffect(() => {
    const currentElement = editWrapper.value
    if (currentElement === null) return
    if (isFull.value) {
      const { left, top, width, height } =
        currentElement.getBoundingClientRect()
      prevPosi.width = `${width}px`
      prevPosi.height = `${height}px`
      prevPosi.left = `${left}px`
      prevPosi.top = `${top}px`
      prevPosi.transition = "0.2s"

      const { style } = currentElement
      style.width = "100%"
      style.height = "calc(100% - 108px)"
      style.top = "0"
      style.left = "0"
      style.transition = "0.2s"
    } else {
      Object.assign(currentElement.style, prevPosi)
    }
    setTimeout(() => (currentElement.style.transition = "none"), 200)
  })
  const fullHandler = (flag: boolean) => (isFull.value = flag)
  return {
    isFull,
    fullHandler,
  }
}

type ResizeDirection = "lt" | "lb" | "rt" | "rb"
export const useModalResize = (
  editWrapper: Ref<HTMLElement | null>
): [(direction: ResizeDirection) => void, Ref<boolean>] => {
  type OriginalPositions = {
    left: number
    right: number
    top: number
    bottom: number
  }
  const caculateSize = (
    direction: ResizeDirection,
    e: MouseEvent,
    positions: OriginalPositions
  ) => {
    const { clientX, clientY } = e
    const { left, right, top, bottom } = positions
    const rightWidth = clientX - left
    const leftWidth = right - clientX
    const bottomHeight = clientY - top
    const topHeight = bottom - clientY
    const topOffset = clientY
    const leftOffset = clientX
    switch (direction) {
      case "lt":
        return {
          width: leftWidth,
          height: topHeight,
          top: topOffset,
          left: leftOffset,
        }
      case "rt":
        return {
          width: rightWidth,
          height: topHeight,
          top: topOffset,
        }
      case "lb":
        return {
          width: leftWidth,
          height: bottomHeight,
          left: leftOffset,
        }
      case "rb":
        return {
          width: rightWidth,
          height: bottomHeight,
        }
      default:
        break
    }
  }
  const ing = ref(false)
  const startResize = (direction: ResizeDirection) => {
    ing.value = true
    const currentElement = editWrapper.value as HTMLElement
    const { left, right, top, bottom } = currentElement.getBoundingClientRect()
    const handleMove = (e: MouseEvent) => {
      const size = caculateSize(direction, e, { left, right, top, bottom })
      const { style } = currentElement
      if (size) {
        if (size.width >= 1000) {
          style.width = size.width + "px"
          if (size.left) {
            style.left = size.left + "px"
          }
        }
        if (size.height >= 600) {
          style.height = size.height + "px"
          if (size.top) {
            style.top = size.top + "px"
          }
        }
      }
    }
    const handleMouseUp = () => {
      ing.value = false
      document.removeEventListener("mousemove", handleMove)
      document.removeEventListener("mouseup", handleMouseUp)
    }
    document.addEventListener("mousemove", handleMove)
    document.addEventListener("mouseup", handleMouseUp)
  }
  return [startResize, ing]
}

export const useDrag = (
  editWrapper: Ref<HTMLElement | null>
): [(e: MouseEvent) => void, Ref<boolean>] => {
  const caculateMovePosition = (e: MouseEvent) => {
    const left = e.clientX - gap.x
    const top = e.clientY - gap.y
    return {
      left,
      top,
    }
  }
  const gap: { x: number; y: number } = {} as any
  const ing = ref(false)
  const startMove = (e: MouseEvent) => {
    ing.value = true
    const currentElement = editWrapper.value
    if (currentElement) {
      const { left, top } = currentElement.getBoundingClientRect()
      gap.x = e.clientX - left
      gap.y = e.clientY - top
    }
    const handleMove = (e: MouseEvent) => {
      const { left, top } = caculateMovePosition(e)
      if (currentElement) {
        currentElement.style.top = top + "px"
        currentElement.style.left = left + "px"
      }
    }
    const handleMouseUp = () => {
      ing.value = false
      document.removeEventListener("mousemove", handleMove)
      document.removeEventListener("mouseup", handleMouseUp)
    }
    document.addEventListener("mousemove", handleMove)
    document.addEventListener("mouseup", handleMouseUp)
  }
  return [startMove, ing]
}

export const useContextMenu = (
  actions: Action[],
  handler: (data: Action) => void
) => {
  return useMenu((ev) => {
    openAppMenu(actions, handler, {
      left: `${ev.clientX}px`,
      top: `${ev.clientY}px`,
    })
  })
}
