import { defineComponent, PropType, CSSProperties, ref, Ref, toRefs } from "vue"
import ResizeModal from "./ResizeModal.vue"
import { AppData } from "@/service"
import { resolveUrl } from "@/utils/utils"

export default defineComponent({
  props: {
    app: Object as PropType<AppData>,
    xstyle: Object as PropType<Ref<CSSProperties>>,
    refmd: Object as PropType<{ value?: Ref<any> }>,
    fill: Object as PropType<Ref<boolean>>,
  },
  emits: ["close", "hide", "click"],
  setup(props, { emit }) {
    const { app, refmd } = toRefs(props)
    const refIns = ref()
    if (refmd.value) refmd.value.value = refIns

    return () => (
      <>
        <ResizeModal
          ref={refIns}
          xstyle={props.xstyle}
          header={{ icon: app.value?.appIcon, title: app.value?.appName }}
          fill={props.fill}
          onClose={() => emit("close")}
          onHide={() => emit("hide")}
          onClick={() => emit("click")}
        >
          {app.value?.linkAddress && (
            <iframe
              class={"app-iframe"}
              src={resolveUrl(app.value?.linkAddress)}
              frameborder="0"
            ></iframe>
          )}
        </ResizeModal>
      </>
    )
  },
})
