import { createVNode, render, ref, DefineComponent } from "vue"
import { app } from "@/main"
const openModal = (
  xstyle: Record<string, any> = {},
  close: () => void = () => {},
  Component: DefineComponent<any, any, any, any, any, any, any, any, any, any>,
  duration = 500
) => {
  const container = document.createElement("div")
  container.style.position = "absolute"
  container.style.top = "0"
  container.style.bottom = "108px"
  container.style.width = "100%"
  container.style.zIndex = "99"
  function handler(this: HTMLDivElement, ev: MouseEvent) {
    const target = ev.target as HTMLElement
    const p1 = this === target
    // const p2 = Array.prototype.includes.call(target.classList, "item")
    // const p3 = Array.prototype.includes.call(
    //   (target.parentNode as HTMLElement).classList,
    //   "item"
    // )
    if (p1) {
      ;(close || _close)()
      this.removeEventListener("click", handler)
    }
  }
  container.addEventListener("click", handler)

  const _close = () => {
    animate.value = [""]
    render(null, container)
    document.body.removeChild(container)
    // animate.value = ["animate__backOutDown"]
    // setTimeout(() => {
    //   render(null, container)
    //   document.body.removeChild(container)
    // }, duration)
  }

  const animate = ref(["animate__fadeInUp"])
  const vm = createVNode(<Component onClose={close || _close} />, {
    xstyle,
    animate,
  })
  vm.appContext = app._context
  render(vm, container)
  document.body.appendChild(container)
  return _close
}

export default openModal
