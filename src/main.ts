import "./styles/common.less"
import "swiper/swiper.min.css"
import "animate.css"
import "swiper/modules/pagination/pagination.min.css"
import { createApp, computed } from "vue"
import App from "./App.vue"
import router from "./router"
import "./init"
import "./registerMessage"
import store, { key } from "./store"
import { prefixImgUrl } from "./plugins"

export const app = createApp(App)
app
  .use(store, key)
  .use(router)
  .use(
    prefixImgUrl,
    computed(() => store.state.setting.imageUrl)
  )
  .mount("#app")

/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-ignore
window.mystore = store
