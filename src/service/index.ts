import request, { requestNewFeat } from "@utils/request"

export type AppData = Partial<{
  accessType: string
  appDefault: string
  appDesc: string
  appIcon: string
  appId: string
  appName: string
  appOrder: string
  appStatus: string
  appUseStatus: boolean
  appVersion: string
  company: string
  componentCode: string
  createBy: string
  createByName: string
  createTime: number
  englishName: string
  id: string
  imagePowerType: number
  insertType: string
  instanceId: string
  instanceName: string
  isOpenEnter: string
  labelId: string
  labelName: string
  linkAddress: string
  normalAppName: string
  openWith: string
  pcFlag: boolean
  regionalization: string
  reviewedTime: number
  updateTime: number
  isPerson?: boolean
}>
export const getDeskAppByLabelId = (params: { labelId: string }) => {
  return request<{
    datas: AppData[]
  }>({
    url: "/desktopApps/appList210",
    params,
  })
}

export type AppDataFromAll = Partial<{
  appDefault: string
  appDesc: string
  appIcon: string
  appName: string
  appStatus: string
  applicationId: string
  createTime: number
  flag: boolean
  id: string
  isOpenEnter: string
  linkAddress: string
  openWith: string
  orderBy: number
  sort: number
}>
export type GetAllAppParams = {
  topicId: number
  topicType: number
  isAllAppLabel: number
}
export const getAllApp = (params: GetAllAppParams) => {
  return request<{
    datas: AppDataFromAll[]
  }>({
    url: "/desktopLabel/deskapp/getDeskAppByLabelId",
    params,
  })
}

export type GetTopicAppDetailParams = {
  topicId: number
  topicType: number
  appId: string
}
export const getTopicAppDetail = (params: GetTopicAppDetailParams) => {
  return request<{
    data: {
      data: {
        appIcon: string
        appId: string
        appName: string
        id: string
        linkAddress: string
      }
    }
  }>({
    url: "/topic/getTopicAppDetail",
    params,
  })
}

export const getPersonalCenterUrl = () => {
  return request<{
    data: string
  }>({
    url: "/desktopMenu/getPersonalCenterUrl",
  })
}

export type TaskBar = {
  appIcon: string
  appName: string
  applicationId: string
  linkAddress: string
  isLock?: boolean
  isOpen?: boolean
  [key: string]: any
}
export const saveTaskBar = (data: TaskBar[]) => {
  return requestNewFeat({
    url: "/desktop-taskbar/save",
    method: "POST",
    data,
  })
}
export const getTaskBar = () => {
  return requestNewFeat<TaskBar[]>({
    url: "/desktop-taskbar/list",
    method: "GET",
  })
}

export const deleteDeskAppById = (data: string[]) => {
  return request({
    url: "/desktopLabel/deskapp/deleteDeskAppById",
    method: "POST",
    data,
  })
}

export type TopicData = {
  id: number
  platformName: string
  personalLabels: 0 | 1
}
export const selectTopicDetailByUrl = (params: {
  topicUrl: string
  topicType: number
}) => {
  return request<{ data: { data: TopicData } }>({
    url: "/topic/selectTopicDetailByUrl",
    method: "GET",
    params,
    topic: false,
  })
}

export const getLoginMsg = () => {
  return request<{ loginBackground: string }>({
    url: "/desktopBackground/getLoginMsg",
    method: "GET",
    params: {
      topicType: 2,
    },
  })
}

export type IpcContent = { icpContent: string; icpLink: string }
export const getIpcContent = () => {
  return requestNewFeat<IpcContent>({
    url: "/desktop-management/getContent",
    method: "GET",
  })
}
