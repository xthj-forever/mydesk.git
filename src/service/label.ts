import request, { requestNewFeat } from "@utils/request"
import { AppData } from "./index"
// 标签列表
export const getCategoryListClient = (params: {
  topicId: number
  topicType: number
}) => {
  return request({
    url: "/desktopLabel/label/getSuperBATopLabelList",
    method: "get",
    params,
    topic: true,
  })
}
// 新建个人标签
export const addLabel = (data: {
  appIds: never[] | string[]
  id: string
  labelIcon: string
  labelName: string
  isAllAppLabel: number
  topicId: number
  topicType: number
}) => {
  return request({
    url: "/desktopLabel/label/addLabel",
    method: "post",
    data,
  })
}
// 选择个人标签
// desktopLabel/deskapp/getDeskAppByLabelId?labelId=f915bb8ac5a443638745fe7bb6f67659&topicId=1&topicType=2
export const getDeskAppByLabelIdPerson = (params: {
  labelId: string
  topicId: number
  topicType: number
}) => {
  return request<{ datas: AppData[] }>({
    url: "/desktopLabel/deskapp/getDeskAppByLabelId",
    method: "get",
    params,
  })
}

// 可添加应用列表
// GET
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopLabel/deskapp/list?topicId=1&topicType=2&labelId=0ec69c3830504eb697085598a086a7a7
export type App = {
  appDefault: string
  appDesc: string
  appIcon?: string
  appId: string
  appName: string
  appOrder: string
  appStatus: string
  flag: boolean
  id: string
  isCheck: string
  isOpenEnter: string
  linkAddress?: string
  openWith: string
  readOnly: string
  [key: string]: any
}
export const addLabelList = (params: {
  topicId: number
  topicType: number
  labelId: string
}) => {
  return request<{ datas: App[] }>({
    url: `/desktopLabel/deskapp/list`,
    method: "get",
    params,
  })
}
export type addDeskApp = {
  applicationId: string
  labelId: string
}
// 标签下添加应用
// POST
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopLabel/deskapp/addDeskAppByLabelId?labelId=0ec69c3830504eb697085598a086a7a7
export const addDeskAppByLabelId = (data: {
  labelId: string
  datas: addDeskApp[]
}) => {
  return request({
    url: `/desktopLabel/deskapp/addDeskAppByLabelId?labelId=${data.labelId}`,
    method: "post",
    data: data.datas,
  })
}

// 获取独立标签打开设置
export const getOpenWith = (params: { key: string }) => {
  return request<{ id: number; key: string; value: string }>({
    url: `/personalityDesktopSet/getOpenWith`,
    method: "get",
    params,
  })
}
//设置独立标签打开设置
export const updateOpenWith = (value: string) => {
  return request({
    url: `/personalityDesktopSet/updateOpenWith?key=appOpenWith&value=${value}`,
    method: "post",
  })
}

//设置获取浏览模式
export const getBrowseMode = () => {
  return requestNewFeat<{
    accountId: string
    browseMode: number
    createTime: string
    updateTime: string
  }>({
    url: `/desktop-setting/getBrowseMode`,
    method: "get",
  })
}
//设置更新浏览模式
export const updateBrowseMode = (browseMode: number) => {
  return requestNewFeat({
    url: `/desktop-setting/updateBrowseMode/${browseMode}`,
    method: "post",
  })
}
// 背景相关
// 设置背景图片列表
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopBackground/user/list?topicId=1&topicType=2&type=1
export const desktopBackgroundList = (params: {
  topicId: number
  topicType: number
  type: number
}) => {
  return request({
    url: `/desktopBackground/user/list`,
    method: "get",
    params,
    topic: true,
  })
}
// 获取当前使用桌面背景图片地址
// GET
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopBackground/uesDesktopBackground?type=1&topicId=1&topicType=2
export const getNowBgc = () => {
  return request({
    url: `/desktopBackground/uesDesktopBackground?type=1&topicType=2`,
    method: "get",
    topic: true,
  })
}
// 设置选择背景图片
// POST
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopBackground/uesDesktopBackground/0?topicId=1&topicType=2&type=1
export const uesDesktopBackground = (id: number) => {
  return request({
    url: `/desktopBackground/uesDesktopBackground/${id}?&topicType=2&type=1`,
    method: "post",
    topic: true,
  })
}
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopBackground/uesDesktopBackground/0?topicId=1&topicType=2&type=1

// 设置删除背景图片
// POST
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopBackground/desktopBackground/236
export const deleteBgc = (id: number) => {
  return request({
    url: `/desktopBackground/desktopBackground/${id}`,
    method: "POST",
  })
}
// 设置上传背景图片
// POST
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopBackground/addUserDesktopBackground?topicId=1&topicType=2&type=1
export const addUserDesktopBackground = (data: FormData, callback: any) => {
  return request({
    url: `/desktopBackground/addUserDesktopBackground?topicType=2&type=1`,
    method: "POST",
    data,
    onUploadProgress: (progressEvent) => {
      callback(progressEvent)
    },
  })
}

// 获取图片服务器地址
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/topic/getZhongtaiImageUrl
export const getZhongtaiImageUrl = () => {
  return request<{ data: string }>({
    url: `/topic/getZhongtaiImageUrl`,
    method: "get",
  })
}
// 设置更新个人标签
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopLabel/label/updateLabelById
export const updateLabelById = (data: any) => {
  return request<{ data: string }>({
    url: `/desktopLabel/label/updateLabelById`,
    method: "post",
    data,
  })
}
// 设置删除个人标签
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopLabel/label/deleteLabel?id=0ec69c3830504eb697085598a086a7a7
export const deleteLabel = (id: string) => {
  return request<{ data: string }>({
    url: `/desktopLabel/label/deleteLabel?id=${id}`,
    method: "post",
  })
}
/**
 * 消息
 */
//  消息列表
//  http://testbuild.dataos.top/dataos-back/api/v2/dataos/umsMsg/getMsgList?pageNumber=1&pageSize=10&status=0
type msgType = {
  [key: string]: string | number
}
export const getMsgList = (params: {
  pageSize: number
  pageNumber: number
  status: number
}) => {
  return request<{
    data: {
      datas: msgType[]
      totalCounts: number
    }
  }>({
    url: `/umsMsg/getMsgList`,
    method: "get",
    params,
  })
}
// 删除通知
// POST
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/umsMsg/readMsg
// Body传参
// {"messageIds":"24e5f3c60f154dff91b67f800402260220220621T114144895"}
export const deleteMsg = (data: { messageIds: string }) => {
  return request({
    url: `/umsMsg/readMsg`,
    method: "post",
    data,
  })
}
// 清空通知
// POST
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/umsMsg/emptyMsgList
export const emptyMsgList = () => {
  return request({
    url: `/umsMsg/emptyMsgList`,
    method: "post",
  })
}
/**
 * 个人中心
 */
//  获取头像地址
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/accounts/loginPhoto
export const loginPhoto = () => {
  return request({
    url: `/accounts/loginPhoto`,
    method: "get",
  })
}
// 用户信息
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/accounts/getAccountByToken
export const getAccountByToken = () => {
  return request<{ data: any }>({
    url: `/accounts/getAccountByToken`,
    method: "get",
  })
}
// 用户信息
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/accounts/getAccountByToken
export const getUnReadMsgCount = () => {
  return request<{ data: number }>({
    url: `/msg/getUnReadMsgCount`,
    method: "get",
  })
}
// 小工具列表
// GET
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/desktopApps/toolsList?topicId=1&topicType=2
export const getToolsList = () => {
  return request({
    url: `/desktopApps/toolsList?&topicType=2`,
    method: "get",
    topic: true,
  })
}

// POST
// 登录
// http://testbuild.dataos.top/dataos-back/api/v2/dataos/auth/login
export const login = (data: {
  accountName: string
  accountPasswd: string
  isLocalRegion: boolean
}) => {
  return request({
    url: `/auth/login`,
    method: "post",
    data,
  })
}
// http://testbuild.dataos.top/dataos-back/api/v2/penguin/auth/logout
export const logout = () => {
  return request({
    url: `/auth/logout`,
    method: "post",
  })
}

// 退出登录
