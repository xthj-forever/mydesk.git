import * as handler from "./handler"

// 恕我直言，dataos原来的代码写的都是垃圾，没法看
// 注册通讯message
export type EventData = {
  type: "openApp" | "openSystemApp" | "refreshDesktopList" | "loginOut"
  newData: {
    appData: any
  }
}
window.addEventListener("message", function (event: MessageEvent<EventData>) {
  if (handler[event.data.type]) handler[event.data.type](event.data)
})
