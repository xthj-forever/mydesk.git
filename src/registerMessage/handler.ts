import { EventData } from "./index"
import {
  launchApp,
  getAppsByLabel,
  loginOut as _loginOut,
  queryAppByLink,
} from "@/utils/utils"

export function openApp(data: EventData) {
  const openData = data.newData.appData
  launchApp({
    appIcon: openData.appIcon,
    linkAddress: openData.linkAddress,
    appName: openData.name,
    applicationId:
      openData.id ||
      queryAppByLink(openData.linkAddress)?.applicationId ||
      String(Date.now()),
  })
}

export function openSystemApp(data: EventData) {
  const openData = data.newData.appData
  launchApp({
    appIcon: openData.appIcon,
    linkAddress: `${openData.linkAddress}?fromModule=${openData.secondMenu}`,
    appName: openData.name,
    applicationId:
      openData.id ||
      queryAppByLink(openData.linkAddress)?.applicationId ||
      String(Date.now()),
  })
}

export function refreshDesktopList() {
  getAppsByLabel(false)
}

export function loginOut() {
  _loginOut()
}
