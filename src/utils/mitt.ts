import mitt from "mitt"
import { TopicData } from "@/service"

type tabData = {
  labelId: string
}

type Events = {
  DeskTabChange: tabData
  ResolveTopicData: TopicData
  LabelChange: any
}

const emitter = mitt<Events>()

type emit = <Key extends keyof Events>(type: Key, event: Events[Key]) => void
export const emit: emit = function (...args) {
  return emitter.emit(...args)
}

export default emitter
