import axios, { AxiosRequestConfig } from "axios"
import CONFIG from "@/config"
import { message } from "ant-design-vue"
import router from "@router/index"
import { getTopicData } from "@/utils/utils"
const instance = axios.create({
  baseURL: CONFIG.baseURL,
})

interface ReqConfig extends AxiosRequestConfig {
  tip?: boolean
  topic?: boolean
}

// type ExtraPromise<T> = T extends Promise<infer U> ? U : T
// T传接口的result的格式
const request = async <T>(config: ReqConfig) => {
  const {
    tip = true, // 请求出错提示
    topic = true, // 要不要topic参数
    ...config_
  } = config
  // let topicData: ExtraPromise<ReturnType<typeof getTopicData>>
  if (topic) {
    const topicData = await getTopicData()
    config_.params = config_.params || {}
    Object.assign(config_.params, { topicId: topicData?.id })
    if (["POST", "post"].includes(config_.method as string)) {
      if (config_.data && Reflect.has(config_.data, "topicId")) {
        config_.data.topicId = topicData?.id
      }
    }
  }
  return instance({
    headers:
      sessionStorage.getItem("accessToken") &&
      sessionStorage.getItem("accountId")
        ? {
            accessToken: sessionStorage.getItem("accessToken") as string,
            accountId: sessionStorage.getItem("accountId") as string,
          }
        : {},
    ...config_,
  })
    .then(({ data }) => {
      if (config.responseType === "blob") {
        return data as T
      } else {
        const { code, result } = data
        if (code === 200) {
          return result as T
        } else {
          if (code === 11030113 || code === 401) {
            router.replace("/login")
          }
          return Promise.reject(data)
        }
      }
    })
    .catch((error) => {
      if (tip && error) message.error(error.message || "请求出错！")
      return Promise.reject(error)
    })
}

export default request

export const requestNewFeat: typeof request = (config) => {
  const _config: ReqConfig = { ...config, baseURL: CONFIG.baseURL2 }
  return request(_config)
}
