import { TaskBar } from "@/service"
import { logout } from "@service/label"
import store from "@/store"
import config from "@/config"
import qs from "qs"
import router from "@/router"
import { Modal } from "ant-design-vue"
import { createVNode } from "vue"
import { ExclamationCircleOutlined } from "@ant-design/icons-vue"
import emitter from "@/utils/mitt"

// 给url中插入两个参数，插入search中和hash中
export const insertParams = (
  src: string,
  parmas: Record<string, any>,
  type: "insertSearch" | "insertHash" | "insertAll" = "insertAll"
) => {
  // 匹配url中的参数
  const reg =
    /([^#?]*)(\?[^#]*)?(#[^?]*(\?([^=&#]*=[^=&#]*&)*([^=&#]*=[^=&#]*)?)?)?/
  const match = src.match(reg) || []
  const paramsStr = qs.stringify(parmas)

  // search参数
  if (["insertAll", "insertSearch"].includes(type)) {
    let search = match[2]
    if (search) {
      search += `${search.length > 1 ? "&" : ""}${paramsStr}`
      src = src.replace(match[2], search)
    } else {
      src = src.replace(match[1], `${match[1]}?${paramsStr}`)
    }
  }

  // hash参数
  if (["insertAll", "insertHash"].includes(type)) {
    if (match[3]) {
      if (match[4]) {
        let match3 = match[3]
        let match4 = match[4]
        match3 = match3.replace(match4, (match4 += `&${paramsStr}`))
        src = src.replace(match[3], match3)
      } else {
        src += `?${paramsStr}`
      }
    } else {
      src += `#/?${paramsStr}`
    }
  }

  return src
}

export const _resolveUrl = (url: string, params: Record<string, any> = {}) => {
  const reg = /^https?:\/\/.+/
  if (!reg.test(url))
    url = `${
      process.env.NODE_ENV === "development" ? config.subAppBaseUrl : ""
    }${url}`
  return insertParams(url, params, "insertHash")
}
export const resolveUrl = (url: string, params: Record<string, any> = {}) => {
  return _resolveUrl(
    url,
    Object.assign(
      {
        accountToken: sessionStorage.getItem("accessToken"),
        accountId: sessionStorage.getItem("accountId")
      },
      params
    )
  )
}

export const getParentElement = (
  parentclass: string,
  dom: HTMLElement | null
) => {
  let parent: HTMLElement | null = null
  while (dom && !parent) {
    if (dom.classList.contains(parentclass)) {
      parent = dom
    } else {
      dom = dom.parentElement
    }
  }
  return parent
}

export const launchApp = (app: Partial<TaskBar>) => {
  if (store.state.setting.openMode === "1") {
    window.open(resolveUrl(app.linkAddress as string), "_blank")
    return
  }
  // 1. 打开应用弹窗
  store.dispatch("setAppWin", {
    data: app,
    type: "open"
  })
  // 2. 任务栏控制
  store.dispatch("setRealTaskBar", {
    data: { ...app, isOpen: true } as TaskBar
  })
  store.dispatch("setOpenApphistories", {
    type: "add",
    applicationId: app.applicationId
  })
}

export const clearApp = (app: TaskBar) => {
  // 1. 关闭应用弹窗
  store.dispatch("setAppWin", {
    data: app,
    type: "close"
  })
  // 2. 任务栏控制
  store.dispatch("setRealTaskBar", {
    data: app,
    type: "delete"
  })
  store.dispatch("setOpenApphistories", {
    type: "delete",
    applicationId: app.applicationId
  })
}

export const hideApp = (app: TaskBar) => {
  // 1. 影藏应用弹窗
  store.dispatch("setAppWin", {
    data: app,
    type: "hide"
  })
  // 2. 任务栏控制
  store.dispatch("setOpenApphistories", {
    type: "delete",
    applicationId: app.applicationId
  })
}

export const getAppsByLabel = (clearFlag = true) => {
  const currentLabel = store.state.deskTabel.currentLabel
  const payload = {
    isValidate: true,
    labelId: store.getters.labelId,
    isPerson: Reflect.has(currentLabel, "id")
  }
  clearFlag && store.commit("setAppsByLabel", [])
  store.dispatch("getAppsByLabel", payload)
  return payload
}

export const loginOut = () => {
  Modal.confirm({
    content: "确认是否退出登录？",
    icon: createVNode(ExclamationCircleOutlined),
    onOk() {
      return logout()
        .then(() => {
          sessionStorage.removeItem("accessToken")
          sessionStorage.removeItem("accountId")
          router.replace("/login")
        })
        .catch(() => {})
    },
    okText: "确认",
    cancelText: "取消",
    centered: true
  })
}

export const queryAppByLink = (linkAddress: string) => {
  return store.state.apps.allApps.find((app) => app.linkAddress === linkAddress)
}

// 读取file文件base64格式
export function getBase64(file: File | undefined): Promise<string> {
  return new Promise((resolve, reject) => {
    if (!file) return resolve("")
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result as string)
    reader.onerror = (error) => reject(error)
  })
}

// 计算uuid
export const uuid = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    const r = (Math.random() * 16) | 0
    /* eslint-disable */
    const v = c === "x" ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })
}

// 手动加载js
export const fetchScript = (src: string): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    // 已经加载过了直接过
    if (document.getElementById(src)) {
      resolve(true)
    } else {
      const scriptDom = document.createElement("script")
      scriptDom.src = src
      scriptDom.id = src
      scriptDom.type = "text/javascript"
      scriptDom.charset = "utf-8"
      scriptDom.onload = () => resolve(true)
      scriptDom.onerror = reject
      document.body.appendChild(scriptDom)
    }
  })
}

// 在hash里面插入query参数
export const hackQueryByHash = (key: string, value: string | number) => {
  const hash = location.hash
  const reg = /\?([^=&#]*=[^=&#]*&)*([^=&#]*=[^=&#]*)*/ // 从url中匹配出query参数
  const match = hash.match(reg)
  let hash_ = ""
  if (match) {
    if (match[0].includes(key)) {
      hash_ = hash.replace(
        match[0],
        match[0].replace(new RegExp(`${key}=[^=&#]*`), `${key}=${value}`)
      )
    } else {
      hash_ = hash.replace(match[0], `${match[0]}&${key}=${value}`)
    }
  } else {
    const subHashIndex = hash.slice(1).indexOf("#")
    if (subHashIndex > -1) {
      hash_ = `${hash.slice(0, subHashIndex)}?${key}=${value}${hash.slice(
        subHashIndex
      )}`
    } else {
      hash_ = `${hash}?${key}=${value}`
    }
  }
  location.hash = hash_
}
// 第一次立即执行，后续延迟执行最后一个
// export const throttle = (fn: Function, duration: number) => {
// 	let timer: NodeJS.Timeout | null = null // 表示是否存在duration计时
// 	return function (this: ThisType<any>, ...arg: any[]) {
// 		if (timer) {
// 			clearTimeout(timer)
// 			timer = setTimeout(() => {
// 				fn.call(this, ...arg)
// 				timer = null
// 			}, duration)
// 		} else {
// 			fn.call(this, ...arg)
// 			timer = setTimeout(() => timer = null, duration)
// 		}
// 	}
// }

// str: key=value&key=value
export const computeQuery = (str: string): { [key: string]: string } =>
  decodeURIComponent(str)
    .split("&")
    .reduce(
      (query, curr) =>
        curr
          ? {
              ...query,
              [curr.split("=")[0]]: curr.split("=")[1],
            }
          : query,
      {}
    )

// icon
// export const checkAppIcon = (icon: string, prefix: string = config.iconBaseURL) => {
//   return /^(http|https):\/\/.+/.test(icon) ? icon : icon ? `${prefix}/${icon}` : PlaceholderImageUrl
// }

// icon
// export const checkAppIcon = (icon: string, prefix: string = window.iconPrefix) => {
//   return /^(http|https):\/\/.+/.test(icon) ? icon : icon ? `${prefix}/${icon}` : ''
// }

// a标签下载
export const downloadByAtag = (url: string, fileName: string) => {
  const aDom = document.createElement("a")
  aDom.href = url
  aDom.download = fileName
  document.body.appendChild(aDom)
  aDom.click()
  setTimeout(() => document.body.removeChild(aDom))
}

// 获取topicdata
let isPendingFetchTopicData = false
export const getTopicData = async () => {
  let topicData = store.state.topicData
  if (!topicData) {
    if (isPendingFetchTopicData) {
      topicData = await new Promise((resolve, reject) => {
        emitter.on("ResolveTopicData", (data) => resolve(data))
      })
    } else {
      isPendingFetchTopicData = true
      topicData = await store.dispatch("fetchTopicData")
      emitter.emit("ResolveTopicData", topicData as any)
      emitter.off("ResolveTopicData")
      isPendingFetchTopicData = false
    }
  }
  return topicData
}
