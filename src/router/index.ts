import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router"
import Login from "../views/Login.vue"
import Home from "../views/Home.vue"
import store from "../store"

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

router.beforeEach(async (to) => {
  const token = sessionStorage.getItem("accessToken")
  if (!token && to.name !== "Login") return { name: "Login" }
  if (to.name === "Login") store.dispatch("reset")
})

export default router
