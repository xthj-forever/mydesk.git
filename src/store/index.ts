import { createStore, Store, useStore as baseUseStore } from "vuex"
import { InjectionKey } from "vue"
import setting, { Setting } from "./setting"
import deskTabel, { DeskTab } from "./deskTab"
import apps, { Apps } from "./apps"
import {
  TopicData,
  selectTopicDetailByUrl,
  getIpcContent,
  IpcContent,
} from "@/service"
import config from "@/config"

export const key: InjectionKey<Store<GlobalState>> = Symbol()
export interface GlobalState {
  setting: Setting
  deskTabel: DeskTab
  apps: Apps
  topicData: Partial<TopicData> | undefined
  ipcContent: Partial<IpcContent> | undefined
}

export default createStore<GlobalState>({
  state: {
    topicData: undefined,
    ipcContent: undefined,
  } as GlobalState,
  modules: {
    setting,
    deskTabel,
    apps,
  },
  mutations: {
    setTopicData(state, payload: TopicData) {
      state.topicData = payload
    },
    setIpcContent(state, payload: IpcContent) {
      state.ipcContent = payload
    },
  },
  actions: {
    fetchTopicData({ commit }) {
      const topicUrl_ = location.origin + location.pathname
      const topicUrl =
        process.env.NODE_ENV === "development" ? config.deskAddress : topicUrl_
      return selectTopicDetailByUrl({ topicUrl, topicType: 2 }).then((res) => {
        commit("setTopicData", res.data.data)
        document.title = res.data.data.platformName
        return res.data.data
      })
    },
    fetchIpcContent({ commit }) {
      return getIpcContent().then((res) => commit("setIpcContent", res))
    },
  },
})

export function useStore() {
  return baseUseStore(key)
}
