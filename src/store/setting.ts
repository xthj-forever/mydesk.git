import { Module } from "vuex"
import { GlobalState } from "./index"
import imgUrl from "@/assets/bg1.png"
import loginBgc from "@/assets/loginBgc.png"
import { getZhongtaiImageUrl, getNowBgc } from "@service/label"
import { getLoginMsg } from "@/service/index"
import { CSSProperties } from "vue"

type CustomLabel = {
  id: string
  labelIcon?: string
  labelName?: string
  topicId?: number
  topicType?: number
  [key: string]: any
}
export interface Setting {
  standaloneOpen: boolean
  viewMode: "standard" | "care"
  bgImg: string
  loginBgc: string
  imageUrl: string
  personalLabelList: CustomLabel[]
  openMode: "0" | "1" // 0iframe 1新创
}
const setting: Module<Setting, GlobalState> = {
  state: {
    standaloneOpen: true,
    viewMode: "standard",
    bgImg: `../${imgUrl}`,
    imageUrl: "",
    loginBgc: "",
    personalLabelList: [],
    openMode: "0",
  },
  mutations: {
    setLoginBgc(state, loginBgc: Setting["loginBgc"]) {
      state.loginBgc = loginBgc
    },
    setOpenMode(state, openMode: Setting["openMode"]) {
      state.openMode = openMode
    },
    setViewMode(state, viewMode: Setting["viewMode"]) {
      state.viewMode = viewMode
    },
    setBgImg(state, bgImg: Setting["imageUrl"]) {
      state.bgImg = bgImg
    },
    setImageUrl(state, imageUrl: Setting["imageUrl"]) {
      state.imageUrl = imageUrl
    },
    setPersonalLabelList(
      state,
      personalLabelList: Setting["personalLabelList"]
    ) {
      state.personalLabelList = personalLabelList
    },
  },
  getters: {
    bgImg(state) {
      return state.imageUrl + state.bgImg
    },
    loginBgc(state) {
      return state.loginBgc ? state.imageUrl + state.loginBgc : `../${loginBgc}`
    },
    careModeScale(state): CSSProperties {
      return state.viewMode === "care"
        ? { transform: "scale(1.2)", transition: "0.3s" }
        : { transition: "0.3s" }
    },
  },
  actions: {
    getImageUrl({ commit }) {
      return getZhongtaiImageUrl().then((res) => {
        commit("setImageUrl", res.data)
      })
    },
    getBgc({ commit }) {
      return getNowBgc().then((res) => {
        commit("setBgImg", res)
      })
    },
    getLoginMsg({ commit }) {
      return getLoginMsg().then((res) => {
        if (res.loginBackground) commit("setLoginBgc", res.loginBackground)
      })
    },
  },
}
export default setting
