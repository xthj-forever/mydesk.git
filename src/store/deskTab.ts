import { Module } from "vuex"
import { GlobalState } from "./index"

type SystemLabel = {
  appCount?: number
  labelIcon?: string
  labelId: string
  labelName?: string
  [key: string]: any
}
type CustomLabel = {
  id: string
  labelIcon?: string
  labelName?: string
  topicId?: number
  topicType?: number
  [key: string]: any
}

export interface DeskTab {
  currentLabel: SystemLabel | CustomLabel
}

const deskTabel: Module<DeskTab, GlobalState> = {
  state: {
    currentLabel: {} as any,
  },
  mutations: {
    setCurrentDeskLabel(state, payload: DeskTab["currentLabel"]) {
      state.currentLabel = payload
    },
  },
  getters: {
    labelId(state): string | undefined {
      const currentLabel = state.currentLabel
      return (currentLabel && (currentLabel.labelId || currentLabel.id)) || ""
    },
  },
}
export default deskTabel
