import { Module } from "vuex"
import { GlobalState } from "./index"
import { ref, CSSProperties, computed } from "vue"
import { last, cloneDeep } from "lodash-es"
import {
  AppData,
  getDeskAppByLabelId,
  getAllApp,
  AppDataFromAll,
  getTaskBar,
  saveTaskBar,
  TaskBar,
  getTopicAppDetail,
  getPersonalCenterUrl,
  deleteDeskAppById
} from "../service"
import { getDeskAppByLabelIdPerson } from "../service/label"
import OpenAppWin from "@/components/OpenApp"
import { hideApp, clearApp, launchApp } from "@/utils/utils"

// 应用弹窗集合
const appWinList: { appCtroller: ReturnType<typeof OpenAppWin>; id: string }[] =
  []

export interface Apps {
  // 桌面tab切换下的apps
  appsByLabel: AppData[]
  // 桌面左下角切换下的apps
  allApps: AppDataFromAll[]
  // 当前应用展示情况
  showAppType: "allAPP" | "byLabel"

  topicId: number
  topicType: number
  isAllAppLabel: number

  // 后台返回
  taskBar: TaskBar[]
  // 已开应用 记录历史
  openApphistories: string[]
  // 任务栏实时按钮
  realTaskBar: TaskBar[]
  // z-index
  zIndex: number
  // 个人中心链接
  personalLinkAddress: string
}
export const defaultState: Apps = {
  appsByLabel: [],
  showAppType: "byLabel",
  allApps: [],
  topicId: 1,
  topicType: 2,
  isAllAppLabel: 1,
  taskBar: [],
  openApphistories: [],
  realTaskBar: [],
  zIndex: 9,
  personalLinkAddress: ""
}
const resolveDefault = () => cloneDeep(defaultState)
const apps: Module<Apps, GlobalState> = {
  state: resolveDefault(),
  mutations: {
    setOpenApphistories(state, payload: Apps["openApphistories"]) {
      state.openApphistories = payload
    },
    setRealTaskBar(state, payload: Apps["realTaskBar"]) {
      state.realTaskBar = payload
    },
    setAppsByLabel(state, payload: Apps["appsByLabel"]) {
      state.appsByLabel = payload
    },
    setTaskBar(state, payload: Apps["taskBar"]) {
      state.taskBar = payload
    },
    setAllApps(state, payload: Apps["allApps"]) {
      state.allApps = payload
    },
    setShowAppType(state, payload: Apps["showAppType"]) {
      state.showAppType = payload
    },
    setPersonalLinkAddress(state, payload: Apps["personalLinkAddress"]) {
      state.personalLinkAddress = payload
    }
  },
  getters: {
    appsWithoutSystem(state) {
      return state.allApps.filter((app) => app.appDefault !== "1")
    },
    systemApps(state) {
      return state.allApps.filter((app) => app.appDefault === "1")
    }
  },
  actions: {
    reset() {
      this.state.apps = resolveDefault()
      appWinList.forEach((app) => app.appCtroller.close())
      appWinList.length = 0
    },
    setAppWin(
      { state },
      payload: { data: TaskBar; type: "open" | "close" | "hide" }
    ) {
      const { type, data } = payload
      const appWin = appWinList.find((ctr) => ctr.id === data.applicationId)
      if (type === "open") {
        if (appWin) {
          const style = appWin.appCtroller.style
          if (style) style.value.zIndex = ++state.zIndex
          appWin.appCtroller.toggle(true)
        } else {
          const xstyle = ref<CSSProperties>({ zIndex: state.zIndex })
          const fill = computed(
            () => data.applicationId !== last(state.openApphistories)
          )
          const appCtroller = OpenAppWin({
            xstyle,
            fill,
            app: data,
            cbs: {
              onClick: () => launchApp(data),
              onHide: () => hideApp(data),
              onClose: () => clearApp(data)
            }
          })
          appWinList.push({
            id: data.applicationId,
            appCtroller
          })
        }
      } else if (type === "hide") {
        if (appWin) {
          appWin.appCtroller.toggle(false)
        }
      } else if (type === "close") {
        if (appWin) {
          appWin.appCtroller.close()
          const i = appWinList.findIndex((app) => app.id === data.applicationId)
          appWinList.splice(i, 1)
        }
      }
    },
    setRealTaskBar(
      { state },
      payload: { data: TaskBar; type: "delete" | undefined; posi?: TaskBar }
    ) {
      const { data, type, posi } = payload
      const list = state.realTaskBar
      const item = list.find((app) => app.applicationId === data.applicationId)
      const index = list.findIndex(
        (app) => app.applicationId === data.applicationId
      )
      if (type === "delete") {
        if (item) {
          if (item.isLock) {
            item.isOpen = false
          } else {
            state.realTaskBar = list.filter(
              (e) => e.applicationId !== data.applicationId
            )
          }
        }
      } else if (item) {
        if (posi) {
          const posiIndex = list.findIndex(
            (app) => app.applicationId === posi.applicationId
          )
          const currItem = Object.assign(item, data)
          if (index > posiIndex) {
            list.splice(posiIndex, 0, currItem)
            list.splice(index + 1, 1)
          } else if (index === posiIndex) {
            list.splice(posiIndex, 0, currItem)
            list.splice(index + 1, 1)
          } else {
            list.splice(posiIndex + 1, 0, currItem)
            list.splice(index, 1)
          }
        } else {
          list[index] = Object.assign(item, data)
        }
      } else {
        let index = list.length
        if (posi) {
          index = list.findIndex(
            (app) => app.applicationId === posi.applicationId
          )
        }
        list.splice(index, 0, { ...data })
      }
    },
    setOpenApphistories(
      { state },
      payload: { type: "add" | "delete"; applicationId: string }
    ) {
      if (payload.type === "add") {
        state.openApphistories.push(payload.applicationId) // 不commit了
      } else if (payload.type === "delete") {
        state.openApphistories = state.openApphistories.filter(
          (id) => id !== payload.applicationId
        )
      }
    },
    saveTaskBar({ commit }, payload: TaskBar[]) {
      return saveTaskBar(payload).then(() => commit("setTaskBar", payload))
    },
    getTaskBar({ commit }) {
      return getTaskBar().then((data) => {
        commit("setTaskBar", data)
        commit(
          "setRealTaskBar",
          data.map((d) => ({ ...d, isLock: true }))
        )
      })
    },
    getAppsByLabel(
      { commit },
      payload: { isValidate: boolean; labelId: string; isPerson: boolean }
    ) {
      if (payload.isPerson) {
        return getDeskAppByLabelIdPerson({
          labelId: payload.labelId,
          topicId: 1,
          topicType: 2
        }).then((data) => {
          if (payload.isValidate) {
            const item = {
              isPerson: true
            }
            data.datas.push(item as any)
            commit("setAppsByLabel", data.datas)
          }
        })
      } else {
        return getDeskAppByLabelId({ labelId: payload.labelId }).then(
          (data) => {
            if (payload.isValidate) {
              commit(
                "setAppsByLabel",
                data.datas.map((d) => ({ ...d, applicationId: d.id }))
              )
            }
          }
        )
      }
    },
    getAllApp(
      { commit, state },
      payload: { isValidate: boolean } = { isValidate: true }
    ) {
      return getAllApp({
        topicId: state.topicId,
        topicType: state.topicType,
        isAllAppLabel: state.isAllAppLabel
      }).then((data) => {
        if (payload.isValidate) {
          commit(
            "setAllApps",
            data.datas.filter(
              (d) => d.applicationId !== "4a63895a832b46c09519a2490c320bd3"
            )
          )
        }
      })
    },
    fetchOpenTool({ state }, payload: any) {
      return getTopicAppDetail({
        topicId: state.topicId,
        topicType: state.topicType,
        appId: payload.applicationId
      }).then((res) => {
        launchApp({
          ...res.data.data,
          applicationId: res.data.data.appId,
          appIcon: payload.toolIcon
        })
      })
    },
    fetchPersonalLinkAddress({ commit }) {
      return getPersonalCenterUrl().then((res) =>
        commit("setPersonalLinkAddress", res.data)
      )
    },
    deleteCustomLabelApp({ state, commit }, payload: string[]) {
      return deleteDeskAppById(payload).then(() => {
        const list = state.appsByLabel.filter(
          (app) => !payload.some((id) => id === app.id)
        )
        commit("setAppsByLabel", list)
      })
    }
  }
}
export default apps
