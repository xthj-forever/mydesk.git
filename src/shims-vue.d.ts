/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

interface Window {
  dependDataosBack: string
  desktopthemesserver: string
}

declare module '*.jpg' {
  const src: string;
  export default src;
}

declare module '*.jpeg' {
  const src: string;
  export default src;
}

declare module '*.png' {
  const src: string;
  export default src;
}

declare module '*.module.less' {
  const classes: Readonly<Record<string, string>>;
  export default classes
}

// 提取函数参数[]类型
declare type ExtractFuncArgs<T> = T extends ((...arg: (infer U)) => any) ? U : T
// 提取函数第一个参数之后的参数[]类型
declare type ExtractFuncArgsAfterFirst<T> = T extends ((first: any, ...arg: (infer U)) => any) ? U : T
// 提取数组里的类型
declare type ExtractArr<T> = T extends ((infer U)[]) ? U : T