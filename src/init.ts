import store from "./store/index"
import { computeQuery } from "./utils/utils"
// 获取token和ID search参数
const searchStr = (window.location.href.match(/\?([^#]+)[/#]*/) || ["", ""])[1]
export const initQuery: Record<"accountToken" | "accountId", string> =
  searchStr.includes("accountToken=")
    ? computeQuery(searchStr.slice(searchStr.indexOf("?") + 1))
    : JSON.parse(sessionStorage.getItem("initQuery") || "{}")
const { accountToken, accountId } = initQuery
if (accountToken) {
  sessionStorage.setItem("accessToken", accountToken)
}
if (accountId) {
  sessionStorage.setItem("accountId", accountId)
}
console.log(store, '===========')
// 获取背景设置
store.dispatch("getImageUrl")
store.dispatch("getLoginMsg")
store.dispatch("fetchIpcContent")
