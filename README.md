# 工会桌面主题

## 代码格式化 prettier
vscode 安装插件 prettier

## commit 规范
commit msg 以如下前缀开头提交  
```
[
'build', // 构建
'chore', // 杂项
'ci', // 持续集成
'docs', // 文档
'feat', // 特性/需求
'fix', // 修复缺陷
'perf', // 性能
'refactor', // 重构
'revert', // 撤销
'style', // 样式
'test', // 测试
]
```
例：`git commit -m 'chore: 初始化项目'`

## useStore  
从store目录引入二次处理的useStore
```
import { useStore } from "store/index"
const store = useStore()
```

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
