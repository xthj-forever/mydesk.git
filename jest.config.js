module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  transform: {
    "^.+\\.vue$": "vue-jest"
  },
  moduleFileExtensions: ["ts", "tsx", "vue", "js", "jsx", "json"],
  moduleNameMapper: {
    "@service/(.*)": "<rootDir>/src/service/$1",
    "@assets/(.*)": "<rootDir>/src/assets/$1",
    "@components/(.*)": "<rootDir>/src/components/$1",
    "@router/(.*)": "<rootDir>/src/router/$1",
    "@utils/(.*)": "<rootDir>/src/utils/$1",
    "@views/(.*)": "<rootDir>/src/views/$1",
    "@store/(.*)": "<rootDir>/src/store/$1",
    "@styles/(.*)": "<rootDir>/src/styles/$1",
    "\\.(css|less)$": "<rootDir>/__mocks__/styleMock.js"
  },
  transformIgnorePatterns: [
    "<rootDir>/node_modules/(?!lodash-es|ant-design-vue)"
  ]
}
