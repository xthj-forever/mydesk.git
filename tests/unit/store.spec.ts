import store from "@/store"
import { TopicData, IpcContent } from "@/service"
import { flushPromises } from "@vue/test-utils"
import request_, { requestNewFeat as requestNewFeat_ } from "@utils/request"
import { defaultState } from "@/store/apps"

const request = request_ as jest.Mock
const requestNewFeat = requestNewFeat_ as jest.Mock
// jest.mock("@utils/request", () => {
//   return {
//     __esModule: true,
//     default: jest.fn(),
//     requestNewFeat: jest.fn()
//   }
// })
// jest.mock("@/components/OpenApp", () => {
//   return {}
// })
// jest.mock("@/utils/utils", () => {
//   return {}
// })
// jest.mock("@/store/setting", () => {
//   return {}
// })
// jest.mock("@/store/deskTab", () => {
//   return {}
// })
// jest.mock("@/store/apps", () => {
//   return {}
// })

describe("test vuex store", () => {
  describe("test global state", () => {
    it("should have three modules and one state prop", () => {
      expect(store.state).toHaveProperty("apps")
      expect(store.state).toHaveProperty("setting")
      expect(store.state).toHaveProperty("deskTabel")
      expect(store.state).toHaveProperty("topicData")
      expect(store.state).toHaveProperty("ipcContent")
    })
    it("commit setTopicData should be fine", () => {
      const payload: TopicData = { id: 2, platformName: "test" }
      store.commit("setTopicData", payload)
      expect(store.state.topicData).toEqual(payload)
    })
    it("commit setIpcContent should be fine", () => {
      const payload: IpcContent = { icpLink: "www", icpContent: "test content" }
      store.commit("setIpcContent", payload)
      expect(store.state.ipcContent).toEqual(payload)
    })
    it("dispatch fetchTopicData should be fine", async () => {
      request.mockResolvedValueOnce({ data: { data: 5 } })
      await store.dispatch("fetchTopicData")
      await flushPromises()
      expect(store.state.topicData).toEqual(5)
    })
    it("dispatch fetchIpcContent should be fine", async () => {
      requestNewFeat.mockResolvedValueOnce(123)
      await store.dispatch("fetchIpcContent")
      await flushPromises()
      expect(store.state.ipcContent).toEqual(123)
    })
  })

  describe("test app module", () => {
    it("app module defaultstate", () => {
      expect(store.state.apps).toEqual(defaultState)
    })
    it("commit setOpenApphistories should be fine", () => {
      store.commit("setOpenApphistories", "test")
      expect(store.state.apps.openApphistories).toContain("test")
    })
    it("commit setRealTaskBar should be fine", () => {
      store.commit("setRealTaskBar", "test")
      expect(store.state.apps.realTaskBar).toContain("test")
    })
    it("commit setAllApps should be fine", () => {
      const apps = [
        { id: 1, appName: "test1", appDefault: "1" },
        { id: 2, appName: "test2" }
      ]
      store.commit("setAllApps", apps)
      expect(store.state.apps.allApps).toEqual(apps)
      expect(store.getters.appsWithoutSystem).toEqual([apps[1]])
      expect(store.getters.systemApps).toEqual([apps[0]])
    })
    it("dispath reset should be fine", () => {
      store.dispatch("reset")
      expect(store.state.apps).toEqual(defaultState)
    })
  })
})
