import { VueWrapper, mount } from "@vue/test-utils"
import AppItem from "@/components/AppItem.vue"
import { AppData, AppDataFromAll } from "@service/index"
import { CSSProperties } from "vue"
import store from "@/store"
import { launchApp } from "@/utils/utils"

// jest.mock("@/store", () => ({
//   useStore: () => ({
//     state: {
//       deskTabel: {
//         currentLabel: {}
//       }
//     }
//   })
// }))
// jest.mock("@/components/hooks", () => {
//   return {
//     useContextMenu: jest.fn(() => {
//       return { domRef: "test" }
//     })
//   }
// })
// jest.mock("@/utils/utils", () => {
//   return {
//     __esModule: true,
//     getParentElement: jest.fn(),
//     launchApp: jest.fn()
//   }
// })
// jest.mock("@utils/request", () => {
//   return {
//     __esModule: true,
//     default: jest.fn(),
//     requestNewFeat: jest.fn()
//   }
// })

describe("test AppItem.vue", () => {
  const app: Partial<AppData & AppDataFromAll> = {
    isPerson: false,
    appName: "test"
  }
  const style: CSSProperties = {}
  let wrapper: VueWrapper<any>
  beforeAll(() => {
    wrapper = mount(AppItem, {
      props: {
        app,
        style
      },
      global: {
        stubs: {
          "add-app": {
            template: "<div>child component</div>"
          }
        },
        provide: {
          store
        },
        directives: {
          prefix: {}
        }
      }
    })
  })

  it("render appName correly", () => {
    expect(wrapper.get("p").text()).toBe(app.appName)
  })
  it("render addBtn correly when isPerson prop is true", async () => {
    await wrapper.setProps({ app: { isPerson: true } })
    await wrapper.get("img").trigger("click")
    expect(wrapper.html()).toContain("child component")
  })
  it("trigger lanchApp when click", async () => {
    await wrapper.setProps({ app: { isPerson: false } })
    await wrapper.get(".item").trigger("click")
    expect(launchApp).toHaveBeenCalled()
  })
})
